# Arch Linux Web Key Directory

The Arch Linux [Web Key Directory](https://wiki.gnupg.org/WKD) provides a way
to discover Arch Linux keyring keys via HTTPS.
