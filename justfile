fqdn := "archlinux.org"

create_wkd:
  sq -f wkd generate -s public/ {{fqdn}} /usr/share/pacman/keyrings/archlinux.gpg

verify:
  for file in public/.well-known/openpgpkey/{{fqdn}}/hu/*; do sq inspect $file; done

create: create_wkd
